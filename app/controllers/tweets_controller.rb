class TweetsController < ApplicationController
  def index
    tweets = Tweet.order('created_at DESC').limit(30).offset(pagination_params)

    render json: tweets
  end

  private
  def pagination_params
    params[:pagination].present? ? params[:pagination] : 0
  end
end
