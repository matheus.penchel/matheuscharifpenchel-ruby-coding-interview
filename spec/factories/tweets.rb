FactoryBot.define do
  factory :tweet do
    user { create(:user) }
    body { Faker::Name.name }
  end
end
