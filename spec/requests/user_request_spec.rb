require 'rails_helper'

RSpec.describe "Users", type: :request do

  RSpec.shared_context 'with multiple companies' do
    let!(:company_1) { create(:company) }
    let!(:company_2) { create(:company) }

    before do
      5.times do
        create(:user, company: company_1)
      end
      5.times do
        create(:user, company: company_2)
      end
    end
  end

  RSpec.shared_context 'with specific usernames' do
    let!(:company) { create(:company) }

    before do
      create(:user, company: company, username: 'Gandalf the Gray')
      create(:user, company: company, username: 'Ganjalf the Green')
      create(:user, company: company, username: 'Gandalf the White')
    end
  end

  describe "#index" do
    let(:result) { JSON.parse(response.body) }

    context 'when fetching users by company' do
      include_context 'with multiple companies'

      it 'returns only the users for the specified company' do
        get company_users_path(company_1)
        
        expect(result.size).to eq(company_1.users.size)
        expect(result.map { |element| element['id'] } ).to eq(company_1.users.ids)
      end
    end

    context 'when fetching all users' do
      include_context 'with multiple companies'

      it 'returns all the users' do
        get company_users_path(company_1)

        expect(result.size).to eq(company_1.users.size)
      end
    end

    context 'when providing the correct username' do
      include_context 'with specific usernames'

      it 'returns the collection with the specified user' do
        get company_users_path(company, username: 'Gandalf the Gray')

        expect(result.size).to eq(1)
      end
    end

    context 'when providing a partial username' do
      include_context 'with specific usernames'

      it 'returns a collection with the users that match the given paramater' do
        get company_users_path(company, username: 'gandalf')

        expect(result.size).to eq(2)
      end
    end
  end
end
