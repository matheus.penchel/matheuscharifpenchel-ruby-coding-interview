require 'rails_helper'

RSpec.describe "Tweets", type: :request do

  describe '#index' do
    before do
      100.times do
        create(:tweet)
      end
    end

    context 'when no pagination param is sent' do
      it 'returns the first 30 tweets' do
        get tweets_path

        expect(result.size).to eq(30)
        # should verify that all the result records are really Tweets
        # should verify that it's really the first 30 tweets
      end
    end


    context 'when pagination param is sent' do
      it 'returns the collection accordingly' do
        get tweets_path(pagination: 30)

        expect(result.size).to eq(30)
        # I should check that the tweets 1-30 and 61-90 should not be here
        # and something similar to what I wrote previously
      end
    end
  end

end
